#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Wasserstein Generative Adversarial Network Practices / Experiments.

Created on Thu Feb 11 18:10:45 2021

@author: nhchan, based on a WGAN-GP code example on Keras documention website
authored by A_K_Nain (https://keras.io/examples/generative/wgan_gp/), but with
extensive documentation strings and comments added.
"""

import os
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

# The following line and if block may be able to prevent tensorflow from 
# crashing with error:
# 
#   Could not create cudnn handle: CUDNN_STATUS_INTERNAL_ERROR
# 
# (Source: https://www.tensorflow.org/guide/gpu and 
#  https://github.com/tensorflow/tensorflow/issues/24496)
# gpus = tf.config.list_physical_devices('GPU')
# if gpus:
#     try:
#         # Currently, memory growth needs to be the same across GPUs
#         for gpu in gpus:
#             tf.config.experimental.set_memory_growth(gpu, True)
#         logical_gpus = tf.config.experimental.list_logical_devices('GPU')
#         print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
#     except RuntimeError as e:
#         # Memory growth must be set before GPUs have been initialized
#         print(e)
        

# Global Specifications
IMAGE_SHAPE = (28, 28, 1)
BATCH_SIZE = 512

NOISE_DIM = 128  # Size of the noice (latent-dimension) vector

# ---- START Lines to Probe Dataset (uncomment or copy to console) ---- #
# fMNIST = keras.datasets.fashion_mnist
# (trainX, trainy), (testX, testy) = fMNIST.load_data()

# plt.figure(figsize = (12, 12))
# for i in range(100):
#     plt.subplot(10, 10, 1 + i)
#     plt.axis("off")
#     plt.imshow(trainX[i], cmap = "gray_r")

# plt.show()

# print(f"Number of example images: {len(trainX)}")
# print(f"Number of test images: {len(testX)}")
# print(
#       f"Shape of individual images: {trainX.shape[1:]}"
#       )  # 1st dimension is the batch dim.
# ----  END Lines to Probe Dataset (uncomment or copy to console)  ---- #


# Fashion MNIST data images each have shape (28, 28).
# We build the machine-learning model with RGB (or other) channels in mind,
# and thus need a third dimension.
# To conform with this, we expand the dataset dimension by 1.
# Pixel values also need to be normalised to the range [-1, 1].
# To keep things modular, define a function to carry this out.
def load_fMNIST_data():
    """Load and normalise to [-1, 1] the Fashion MNIST data as numpy arrays."""
    (training_images, _), (_, _) = keras.datasets.fashion_mnist.load_data()
    
    training_images = training_images.reshape(training_images.shape[0],
                                              *IMAGE_SHAPE)
    
    training_images = training_images.astype("float32")
    training_images = (training_images - 127.5) / 127.5
    
    return training_images


# ======================== Convenience Layer-blocks ========================= #

# ---------------------------------------------------------- downsample_block #
def downsample_block(
        x,
        num_filters,
        layer_activation,
        kernel_size=(3, 3),
        strides=(1, 1),
        padding="same",
        use_bias=True,
        use_batchnorm=False,
        use_dropout=False,
        dropout_rate=0.5
):
    """
    Combine Conv2D with activation etc. for convenience.
    
    Parameters
    ----------
    x : tensor
        The tensor to pass into this block, e.g. returned by a keras layer.
    num_filters : int
        The number of kernel filters to use in the convolution.
    layer_activation : activation layer
        The activation function to use for this block (e.g. `LeakyReLU(0.2)`).
    kernel_size : tuple or list of 2 int
        The height and width of the 2D convolution kernel. The default is 
        (3, 3).
    strides : tuple or list of 2 int, or int
        The size of each step that the convolution kernel takes along the 
        height and width. A single int means equal strides along both 
        dimensions. The default is (1, 1).
    padding : "valid" or "same"
        Same as the `padding` keyword argument of the Conv2D class. From its
        documentation, '"valid" means no padding. "same" results in padding 
        evenly to the left/right or up/down of the input such that output 
        has the same height/width dimension as the input'. The default is 
        "same".
    use_bias : boolean
        Same as the `use_bias` keyword argument of the Conv2D class, 
        specifying whether the Conv2D layer uses a bias vector. The default is 
        True.
    use_batchnorm : boolean
        Specify whether or not BatchNormalization() is used prior to 
        activation. The default is False.
    use_dropout : boolean
        Specify whether or not a Dropout() regularisation layer is added in 
        the end. The default is False.
    dropout_rate : float between 0 and 1
        The fraction of input units to drop. The default is 0.5.
        
    Returns
    -------
    x : tensor
        The tensor output of the last layer of the block.
        
    """
    x = layers.Conv2D(
        num_filters,
        kernel_size,
        strides=strides,
        padding=padding,
        use_bias=use_bias
    )(x)
    
    if use_batchnorm:
        x = layers.BatchNormalization()(x)
        
    x = layer_activation(x)
    
    if use_dropout:
        x = layers.Dropout(dropout_rate)(x)
    
    return x


# ------------------------------------------------------------ upsample_block # 
def upsample_block(
        x,
        num_filters,
        layer_activation,
        kernel_size=(3, 3),
        strides=(1, 1),
        up_size=(2, 2),
        padding="same",
        use_bias=True,
        use_batchnorm=False,
        use_dropout=False,
        dropout_rate=0.5
):
    """
    Combine UpSampling2D then Conv2D with activation etc. for convenience.
    
    Parameters
    ----------
    x : tensor
        The tensor to pass into this block, e.g. returned by a keras layer.
    num_filters : int
        The number of kernel filters to use in the convolution.
    layer_activation : activation layer
        The activation function to use for this block (e.g. `LeakyReLU(0.2)`).
    kernel_size : tuple or list of 2 int
        The height and width of the 2D convolution kernel. The default is 
        (3, 3).
    strides : tuple or list of 2 int, or int
        The size of each step in that the convolution kernel takes along the
        height and width. A single int means equal strides along both
        domensions. The default is (1, 1).
    up_size : int, or tuple of 2 int
        The factors to upsample the rows and columns. For example, (2, 3) 
        means to repeat each row 2 times and each column 3 times to achieve
        upsampling. A single int means equal upscampling factor along both
        dimensions. The default is (2, 2).
    padding : "valid" or "same"
        Same as the `padding` keyword argument of the Conv2D class. From its
        documentation, '"valid" means no padding. "same" results in padding 
        evenly to the left/right or up/down of the input such that output 
        has the same height/width dimension as the input'. The default is 
        "same".
    use_bias : boolean
        Same as the `use_bias` keyword argument of the Conv2D class, 
        specifying whether the Conv2D layer uses a bias vector. The default is 
        True.
    use_batchnorm : boolean
        Specify whether or not BatchNormalization() is used prior to 
        activation. The default is False.
    use_dropout : boolean
        Specify whether or not a Dropout() regularisation layer is added in 
        the end. The default is False.
    dropout_rate : float between 0 and 1
        The fraction of input units to drop. The default is 0.5.
        
    Returns
    -------
    x : tensor
        The tensor output of the last layer of the block.
        
    """
    # First, upsample by repeating rows and columns according to `up_size`
    x = layers.UpSampling2D(up_size)(x)
    
    # Then, perform 2D convolutions on the upsampled data
    x = layers.Conv2D(
        num_filters,
        kernel_size,
        strides=strides,
        padding=padding,
        use_bias=use_bias
    )(x)
    
    if use_batchnorm:
        x = layers.BatchNormalization()(x)
        
    x = layer_activation(x)
    
    if use_dropout:
        x = layers.Dropout(dropout_rate)(x)
    
    return x


# ================================= Critic ================================== #

# ---------------------------------------------------------- get_critic_model #
def get_critic_model():
    """Return the critic model of the WGAN."""
    # Input layer
    image_input = layers.Input(shape=IMAGE_SHAPE)
    
    # Due to the use of 4 convolution blocks with strides of (2, 2), we need at
    # least 2^4 pixels on each dimension to avoid "rounding off" pixels due to 
    # the layer output having odd-number pixels before the final convolution.
    # Therefore, use a ZeroPadding2D layer here, keeping things simple.
    x = layers.ZeroPadding2D(padding=2)(image_input)  # 2 pads on each side
    
    # 4 successive convolutional downsampling blocks 
    # Pixel dim: (32 -> 16 -> 8 -> 4 -> 2)
    # Feature maps: (1 -> 64 -> 128 -> 256 -> 512)
    x = downsample_block(
        x,
        64,
        layers.LeakyReLU(0.2),
        kernel_size=(5, 5),
        strides=(2, 2),
        use_dropout=False,
    )
    x = downsample_block(
        x,
        128,
        layers.LeakyReLU(0.2),
        kernel_size=(5, 5),
        strides=(2, 2),
        use_dropout=True,
        dropout_rate=0.3,
    )
    x = downsample_block(
        x,
        256,
        layers.LeakyReLU(0.2),
        kernel_size=(5, 5),
        strides=(2, 2),
        use_dropout=True,
        dropout_rate=0.3,
    )
    x = downsample_block(
        x,
        512,
        layers.LeakyReLU(0.2),
        kernel_size=(5, 5),
        strides=(2, 2),
        use_dropout=False,
    )
    
    # Flatten (then Dropout) and output logit
    x = layers.Flatten()(x)
    x = layers.Dropout(0.2)(x)
    x = layers.Dense(1)(x)
    
    # Put the model together and return it
    c_model = keras.models.Model(inputs=image_input, outputs=x, name="critic")
    return c_model
     

# ================================ Generator ================================ #

# ------------------------------------------------------- get_generator_model #
def get_generator_model():
    """Return the generator model of the WGAN."""
    # Input layer
    noise = layers.Input(shape=(NOISE_DIM,))
    
    # Transform the noise into 256 "feature maps" each having a size of 4 by 4
    x = layers.Dense(4 * 4 * 256, use_bias=False)(noise)
    x = layers.BatchNormalization()(x)
    x = layers.LeakyReLU(0.2)(x)
    x = layers.Reshape((4, 4, 256))(x)
    
    # 3 successive upsampling blocks 
    # Pixel dim: (4 -> 8 -> 16 -> 32)
    # Feature maps: (256 -> 128 -> 64 -> 1)
    x = upsample_block(
        x,
        128,
        layers.LeakyReLU(0.2),
        use_bias=False,
        use_batchnorm=True,
    )
    x = upsample_block(
        x,
        64,
        layers.LeakyReLU(0.2),
        use_bias=False,
        use_batchnorm=True,
    )
    x = upsample_block(
        x,
        1,
        layers.Activation("tanh"),
        use_bias=False,
        use_batchnorm=True,
    )
    
    # Crop the 32 by 32 image to 28 by 28 to fit the real data images
    x = layers.Cropping2D(cropping=2)(x)  # 2 crops on each side
    
    # Put the model together and return it
    g_model = keras.models.Model(inputs=noise, outputs=x, name="generator")
    return g_model


# ============================= Wasserstein GAN ============================= #

# GAN class with training method, inheriting keras.Model as base class
class WGAN(keras.Model):
    """Wasserstein Generative Adversarial Network with Gradient Penalty."""
    
    # Class constructor
    def __init__(
        self,
        critic,
        generator,
        latent_dim,
        critic_extra_training_steps=5,
        gp_weight=10.0,
    ):
        """
        Construct the WGAN class.
        
        Parameters
        ----------
        critic : keras.Model object
            The critic/discriminator model of the WGAN.
        generator : keras.Model object
            The generator model of the WGAN.
        latent_dim : int
            The dimension of the noise-vector input to the generator.
        critic_extra_training_steps : int
            Steps to train the critic/discriminator for every generator-
            training step. The default is 5.
        gp_weight : float
            Weight of the gradient penalty to add to the critic/discriminator
            loss. The default is 10.0.
            
        """
        super().__init__()  # Inheriting the constructor of the base class,..
        # .. and add the following...
        self.critic = critic
        self.generator = generator
        self.latent_dim = latent_dim
        self.critic_extra_training_steps = critic_extra_training_steps
        self.gp_weight = gp_weight
    
    # Compile method
    def compile(
        self,
        c_optimizer,
        g_optimizer,
        c_loss_func,
        g_loss_func
    ):
        """Compile the WGAN model."""
        super().compile()  # Inheriting the compile method of the base class,..
        # .. and add the following...
        self.c_optimizer = c_optimizer
        self.g_optimizer = g_optimizer
        self.c_loss_func = c_loss_func
        self.g_loss_func = g_loss_func
    
    # Gradient Penalty, for use in critic/discriminator loss in training step
    def gradient_penalty(self, batch_size, real_images, fake_images):
        """
        Calculate the gradient penalty.
        
        The gradient penalty is a method to ensure 1-Lipschitz continuity
        in the discriminator/critic mapping function in Wasserstein GAN. 
        It was proposed in Gulrajani et al. (2017):
            https://arxiv.org/abs/1704.00028
        as an alternative to the weights clipping used by the original authors
        of the Wasserstein GAN.
        
        For WGAN,
        
        1. the optimal critic/disciminator mapping needs to be 1-Lipschitz
        continuous (i.e., with slope not exceeding one), and 
        2. the critic/discriminator mapping is a function transforming between
        real and fake images.
        
        That means the gradient of the critic/discriminator mapping with 
        respect to the images everywhere along linear paths between the real 
        and fake images should not exceed 1. In fact, the optimal critic/
        discriminator mapping has slopes of close to 1 (Proposition 1 of 
        Gulrajani et al., 2017). Therefore, the gradient penalty is introduced
        as a squared penalty score of any deviation of the gradient norm from 
        1, and added to the critic/discriminator loss.
        
        Practically, the gradient penalty term calculated herein is then added
        to the loss function of the critic/discriminator, multiplied by a 
        weight factor.
        
        """
        epsilon = tf.random.uniform(
            [batch_size, 1, 1, 1],
            minval=0,
            maxval=1
        )
        interp_image = real_images * epsilon + fake_images * (1 - epsilon)
        
        with tf.GradientTape() as gp_tape:
            
            # Specify the "abscissa" to watch
            gp_tape.watch(interp_image)
            
            # The function/mapping over which the gradient is to be calculated
            critic_score = self.critic(interp_image, training=True)
        
        # Gradients of the critic function/mapping with respect to the 
        # interpolated image as a whole
        grads = gp_tape.gradient(critic_score, [interp_image])[0]
        
        # Norm of the gradients (but not along the batch dimension)
        norm = tf.sqrt(tf.reduce_sum(tf.square(grads), axis=[1, 2, 3]))
        
        # Expected value of (norm - 1) squared
        gp = tf.reduce_mean((norm - 1) ** 2)
        
        return gp
    
    # Customised `train_step` method
    def train_step(self, real_images):
        """
        Lay out the mathematical logic for one step of training.
        
        See documentation for the `train_step` method under the 
        tensorflow.keras.Model class for more information. As in the original
        `train_step` method, the arguments and returns are listed (verbatim) 
        below.
        
        Parameters
        ----------
        real_images : tensor
            A tensor with shape `(b, h, w, c)`, which is `b` images with size
            `h` by `w`, each with `c` colour channels.
        
        Returns
        -------
        A `dict` containing values that will be passed to 
        `tf.keras.callbacks.CallbackList.on_train_batch_end`. Typically, the
        values of the `Model`’s metrics are returned. Example: 
        `{'loss': 0.2, 'accuracy': 0.7}`.
        
        """
        # If more than one positional arguments are passed into the 
        # Model.fit() method, they will be passed as a tuple, `data`, into
        # the `train_step` method.
        if isinstance(real_images, tuple):
            real_images = real_images[0]
            
        # Get the batch size of the data, which is the 1st dimension.
        batch_size = tf.shape(real_images)[0]
        
        # -------------------------- Brief Outline -------------------------- #
        # Following the training "blueprint" of GAN, we incorporate the 
        # gradient penalty into the critic/discriminator loss. The Wasserstein
        # GAN loss functions themselves are defined externally and passed in
        # via the `compile` method.
        # 
        # For each training step:
        # 
        # 1. train the critic/disciminator `critic_extra_training_steps` times
        #   a. construct the Wasserstein GAN critic/discriminator loss
        #   b. add gradient penalty, multipled by `gp_weight`, to the loss
        # 2. train the generator via the critic discriminator
        # 3. return the generator and critic/discriminator loss to be handled
        #    by the Model class machineries, similar to the default 
        #    `train_step` method.
        # ------------------------------------------------------------------- #
        
        # Train the critic...
        for i in range(self.critic_extra_training_steps):
            
            # Generate the noise/latent vector
            random_latent_vectors = tf.random.normal(
                shape=(batch_size, self.latent_dim),
                name="latent_gen_for_critic_training"
            )
            
            # Set up gradient-records context for computing gradients
            with tf.GradientTape() as tape:
                
                # Generate fake images
                fake_images = self.generator(random_latent_vectors, 
                                             training=True)
                
                # Get logits for the fake images from the critic/discriminator
                fake_logits = self.critic(fake_images, training=True)
                
                # Get logits for the real images from the critic/discriminator
                real_logits = self.critic(real_images, training=True)
                
                # Critic/Discriminator loss
                c_cost = self.c_loss_func(real_logits=real_logits, 
                                          fake_logits=fake_logits)
                
                # Calculate the gradient penalty
                gp = self.gradient_penalty(batch_size, 
                                           real_images, 
                                           fake_images)
                
                # Combine the GP and the critic/discriminator loss
                c_loss = c_cost + self.gp_weight * gp
                
            # Get the gradient with respect to the critic/discriminator loss
            c_gradient = tape.gradient(c_loss, self.critic.trainable_variables)
            
            # Update the critic/discriminator model weights using the optimiser
            # passed in through the `compile` method
            self.c_optimizer.apply_gradients(
                zip(c_gradient, self.critic.trainable_variables)
            )
            # See help for `tf.keras.optimizers.??.apply_gradients`, where ??
            # is any optimizer class object.
                
        # Train the generator...
        # Generate the noise/latent vector
        random_latent_vectors = tf.random.normal(
            shape=(batch_size, self.latent_dim),
            name="latent_gen_for_generator_training"
        )
        
        # Set up gradient-records context for computing gradients
        with tf.GradientTape() as tape:
            
            # Generate fake images
            gen_images = self.generator(random_latent_vectors, training=True)
            
            # Get logits for the fake images from the critic/discriminator
            gen_logits = self.critic(gen_images, training=True)
            
            # Generator loss
            g_loss = self.g_loss_func(fake_logits=gen_logits)
            
        # Get the gradients with respect to the generator loss
        g_gradient = tape.gradient(g_loss, self.generator.trainable_variables)
        
        # Update the generator model weights using the optimiser passed in
        # through the `compile` method
        self.g_optimizer.apply_gradients(
            zip(g_gradient, self.generator.trainable_variables)
        )
        # See help for `tf.keras.optimizers.??.apply_gradients`, where ??
        # is any optimizer class object.
        
        # Return the metrics
        return {'critic_loss': c_loss, 'gen_loss': g_loss}
    
            
# ========================== Diagnostics Callback =========================== #

class GANMonitor(keras.callbacks.Callback):
    """
    Create a Keras callback that periodically saves generated images.
    
    (This class is completely copied from the WGAN-GP code example on Keras 
    documention website, authored by A_K_Nain 
    (https://keras.io/examples/generative/wgan_gp/) without modifications.
    
    """
    
    def __init__(self, num_img=6, latent_dim=128):
        """Construct the GANMonitor callback method."""
        self.num_img = num_img
        self.latent_dim = latent_dim

    def on_epoch_end(self, epoch, logs=None):
        """Define the callback method to execute at the end of each epoch."""
        random_latent_vectors = tf.random.normal(
            shape=(self.num_img, self.latent_dim)
        )
        generated_images = self.model.generator(random_latent_vectors)
        generated_images = (generated_images * 127.5) + 127.5
        
        # Create a subdirectory to hold the generated samples,
        # if not already existing
        if not os.path.isdir("generated_samples"):
            os.mkdir("generated_samples")

        for i in range(self.num_img):
            img = generated_images[i].numpy()
            img = keras.preprocessing.image.array_to_img(img)
            img.save(
                "generated_samples/generated_img_{i}_{epoch}.png".format(
                    i=i, 
                    epoch=epoch
                )
            )


# ============================= Loss Functions ============================== #

def critic_loss(real_logits, fake_logits):
    """Calculate the E[D(x)] - E[D(G(x))] part of the WGAN objective."""
    real_loss = tf.reduce_mean(real_logits)
    fake_loss = tf.reduce_mean(fake_logits)
    # Recalling that we are maximising over D, 
    # but the built-in optimisers are always minimising,
    # so we have to reverse the sign of E[D(x)] - E[D(G(x))].
    return fake_loss - real_loss


def generator_loss(fake_logits):
    """Calculate the -E[D(G(x))] part of the WGAN objective."""
    return -tf.reduce_mean(fake_logits)


# =========================================================================== #
# |                               Main Script                               | #
# =========================================================================== #
# Load the data
trainX = load_fMNIST_data()

# Set the number of training epochs
epochs = 20

# Set up the models
c_model = get_critic_model()
g_model = get_generator_model()
wgan = WGAN(
    critic=c_model,
    generator=g_model,
    latent_dim=NOISE_DIM,
    critic_extra_training_steps=5,
    gp_weight=10.0
)

# See help for `tf.keras.Model.call` for details on calling the models.

# Print model summaries
c_model.summary()
g_model.summary()

# Define the optimisers
critic_optimizer = keras.optimizers.Adam(
    learning_rate=0.0002, 
    beta_1=0.5, 
    beta_2=0.9
)
generator_optimizer = keras.optimizers.Adam(
    learning_rate=0.0002, 
    beta_1=0.5, 
    beta_2=0.9
)

# Compile the WGAN model
wgan.compile(
    c_optimizer=critic_optimizer,
    g_optimizer=generator_optimizer,
    c_loss_func=critic_loss,
    g_loss_func=generator_loss
)

# Instantiate the custom callback class, `GANMonitor`
ganmon = GANMonitor(num_img=10, latent_dim=NOISE_DIM)

# Train the model
wgan.fit(trainX, batch_size=BATCH_SIZE, epochs=epochs, callbacks=[ganmon])

# Save the models
wgan.generator.save("wgan-gp_generator.tf", save_format="tf")
wgan.critic.save("wgan-gp_critic.tf", save_format="tf")
wgan.generator.save("wgan-gp_generator.h5")
wgan.critic.save("wgan-gp_critic.h5")
